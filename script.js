new Vue({
  data(){
    return{
      card_type: 'visa',
      visa_length: 'visa13',
      number: 0
    }
  },
  methods:{
    cc_gen() {
      var form = this;
      var pos;
      var str = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      var sum = 0;
      var final_digit = 0;
      var t = 0;
      var len_offset = 0;
      var len = 0;
      var issuer;
      if (form.card_type == 'visa') {
        str[0] = 4;
        pos = 1;
        if (form.visa_length == 'visa16') {
          len = 16;
        }
        else {
          len = 13;
        }
      }
      else if (form.card_type == 'mastercard') {
        str[0] = 5;
        t = Math.floor(Math.random() * 5) % 5;
        str[1] = 1 + t;
        pos = 2;
        len = 16;
      }
      else if (form.card_type == 'american_express') {
        str[0] = 3;
        t = Math.floor(Math.random() * 4) % 4;
        str[1] = 4 + t;
        pos = 2;
        len = 15;
      }
      else {
        str[0] = 6;
        str[1] = 0;
        str[2] = 1;
        str[3] = 1;
        pos = 4;
        len = 16;
      }
      while (pos < len - 1) {
        str[pos++] = Math.floor(Math.random() * 10) % 10;
      }
      len_offset = (len + 1) % 2;
      for (pos = 0; pos < len - 1; pos++) {
        if ((pos + len_offset) % 2) {
          t = str[pos] * 2;
          if (t > 9) {
            t -= 9;
          }
          sum += t;
        }
        else {
          sum += str[pos];
        }
      }
      final_digit = (10 - (sum % 10)) % 10;
      str[len - 1] = final_digit;
      t = str.join('');
      t = t.substr(0, len);
      form.number = t;
    }
  }
}).$mount('#app')
